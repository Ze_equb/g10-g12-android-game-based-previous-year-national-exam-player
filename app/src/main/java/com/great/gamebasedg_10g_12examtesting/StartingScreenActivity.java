package com.great.gamebasedg_10g_12examtesting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartingScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_screen);
        Button buttonStartGame= findViewById(R.id.button_G10_exam);
        buttonStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        } );

    }
    private void startGame(){
        Intent intent=new Intent(this, G10SubjectsActivity.class
        );
        startActivity(intent);
    }
}
