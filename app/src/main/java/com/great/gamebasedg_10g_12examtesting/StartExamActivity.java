package com.great.gamebasedg_10g_12examtesting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartExamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_exam);
        Button buttonStart2005= findViewById(R.id.button_start_exam);
        buttonStart2005.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExam();
            }
        } );
    }
    private void startExam(){
        Intent intent=new Intent(this,MultipleQuestionsActivity.class);
        startActivity(intent);
    }
}
