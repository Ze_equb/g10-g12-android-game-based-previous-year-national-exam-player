package com.great.gamebasedg_10g_12examtesting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class G10SubjectsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g10_subjects);
        Button buttonStartAmharic= findViewById(R.id.button_english);
        buttonStartAmharic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAmharic();
            }
        } );
    }
    private void startAmharic(){
        Intent intent=new Intent(this,G10YearsActivity.class);
        startActivity(intent);
    }
}
