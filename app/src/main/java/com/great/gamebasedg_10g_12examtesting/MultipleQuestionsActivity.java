package com.great.gamebasedg_10g_12examtesting;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

public class MultipleQuestionsActivity extends AppCompatActivity {
    private TextView textViewQuestions;
    private TextView textViewScore;
    private TextView textViewQuestionCount;
    private TextView textViewCountDown;
    private RadioGroup rbGroup;
    private Button rb1;
    private Button rb2;
    private Button rb3;
    private Button rb4;
    private Button buttonConfirmNext;
    private ColorStateList textColorDefaultRb;
    private int score;
    private int questionCounter;
    private int questionCountTotal;
    private Question currentQuestion;
    private boolean answered;

    private List<Question> questionList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_questions);
        textViewQuestions=findViewById(R.id.text_view_question);
        textViewQuestionCount=findViewById(R.id.text_view_question_count);
        textViewCountDown=findViewById(R.id.text_view_countdown);
        textViewScore=findViewById(R.id.text_view_score);
        rbGroup=findViewById(R.id.radio_group);
        rb1=findViewById(R.id.radio_button1);
        rb2=findViewById(R.id.radio_button2);
        rb3=findViewById(R.id.radio_button3);
        rb4=findViewById(R.id.radio_button4);
        buttonConfirmNext=findViewById(R.id.button_confirm_next);
        textColorDefaultRb=rb1.getTextColors();
        ExamDbHelper dbHelper=new ExamDbHelper(this);
        questionList=dbHelper.getAllQuestions();
        questionCountTotal=questionList.size();
        Collections.shuffle(questionList);
        showNextQuestion();
        buttonConfirmNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!answered){
                    if (rb1.isSelected()|| rb2.isSelected() || rb3.isSelected()|| rb4.isSelected()){
                        checkAnswer();
                    }else{
                        Toast.makeText(MultipleQuestionsActivity.this, "Please Select an Answer", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    private  void showNextQuestion(){
        rb1.setTextColor(textColorDefaultRb);
        rb2.setTextColor(textColorDefaultRb);
        rb3.setTextColor(textColorDefaultRb);
        rb3.setTextColor(textColorDefaultRb);
        rbGroup.clearCheck();
        if(questionCounter < questionCountTotal){
            currentQuestion=questionList.get(questionCounter);
            textViewQuestions.setText(currentQuestion.getQuestion());
            rb1.setText(currentQuestion.getOption1());
            rb2.setText(currentQuestion.getOption2());
            rb3.setText(currentQuestion.getOption3());
            rb4.setText(currentQuestion.getOption4());
            questionCounter++;
            textViewQuestionCount.setText("Question:" + questionCounter+"/" +questionCountTotal);
            answered=false;
            buttonConfirmNext.setText("confirm");
        }else{
            finishExam();
        }

    }
    private void checkAnswer(){
        answered=true;
        RadioButton rbSelected=findViewById(rbGroup.getCheckedRadioButtonId());
        int answerNr=rbGroup.indexOfChild(rbSelected) +1;
        if (answerNr==currentQuestion.getAnswerNr()){
            score++;
            textViewScore.setText("Score:" +score);
        }
        showSolution();
    }
    private void showSolution(){
        switch (currentQuestion.getAnswerNr()){
            case 1:
                rb1.setTextColor(Color.GREEN);
                textViewQuestions.setText("Answer 1 is Correct!");
                break;
            case 2:
                rb2.setTextColor(Color.GREEN);
                textViewQuestions.setText("Answer 2 is Correct!");
                break;
            case 3:
                rb3.setTextColor(Color.GREEN);
                textViewQuestions.setText("Answer 3 is Correct!");
                break;
            case 4:
                rb1.setTextColor(Color.GREEN);
                textViewQuestions.setText("Answer 4 is Correct!");
                break;
        }
        if (questionCounter < questionCountTotal){
            buttonConfirmNext.setText("Next");
        }else {
            buttonConfirmNext.setText("Finish");
        }
    }
    private void finishExam(){
        finish();
    }
}
