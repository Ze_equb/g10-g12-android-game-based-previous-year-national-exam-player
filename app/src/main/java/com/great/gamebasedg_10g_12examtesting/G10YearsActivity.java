package com.great.gamebasedg_10g_12examtesting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class G10YearsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g10_years);
        Button buttonStart2005= findViewById(R.id.button_2005);
        buttonStart2005.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start2005();
            }
        } );
    }
    private void start2005(){
        Intent intent=new Intent(this,StartExamActivity.class);
        startActivity(intent);
    }
}
